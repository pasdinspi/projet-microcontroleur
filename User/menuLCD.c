#include "menuLCD.h"

/************
 * initMenu *
 ************/
void initMenu(void){
	
	LCD_Initialization();
	numMenu = 0;
}

void menu(void){
	switch(numMenu){
		
		case 0:
			//Rectangle temps
			LCD_Draw_Rectangle(32,30,50,40,2,0,Black,Yellow);
			sprintf(chaine,"temps");
			LCD_Write_String (38,42,chaine,White,Blue);
		
			//Rectangle jour/nuit
			LCD_Draw_Rectangle(88,30,50,40,2,0,Black,Yellow);
			sprintf(chaine,"J/N");
			LCD_Write_String (102,42,chaine,White,Blue);
		
			//Rectangle température
			LCD_Draw_Rectangle(168,30,50,40,2,0,Black,Yellow);
			sprintf(chaine,"C*");
			LCD_Write_String (186,42,chaine,White,Blue);
			
			//Rectangle heure
			LCD_Draw_Rectangle(32,200,186,40,2,0,Black,Yellow);
			sprintf(chaine,"HH:MM:SS");
			LCD_Write_String (100,212,chaine,White,Blue);
		
		break;
	}
}

