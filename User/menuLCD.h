#include "affichagelcd.h"
#include "touch\ili_lcd_general.h"
#include "touch\lcd_api.h"
#include "touch\touch_panel.h"
#include "global.h"
#include <stdio.h>

/*
 * initMenu()
 * Initialise l'�cran lcd
 */
void initMenu(void);

/*
 * menu()
 * G�re les diff�rents menu sur l'�cran LCD
 */
void menu(void);
